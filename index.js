const express = require("express");
const app = express();
const port = 3000;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

/* /home */
app.get("/home", (req, res) => {
    res.send("Welcome to the home page");
});

/* /users */
const mockUsersDb = [];
mockUsersDb.push({
    username: "johndoe",
    password: "johndoe1234",
});

app.get("/users", (req, res) => {
    res.send(mockUsersDb);
});

/* /delete-user */
app.delete("/delete-user", (req, res) => {
    let message;
    for (let i = 0; i < mockUsersDb.length; i++) {
        if (req.body.username == mockUsersDb[i].username) {
            mockUsersDb.splice(i, 1);
            message = `User ${req.body.username} has been deleted.`;
            break;
        } else {
            message = `User "${req.body.username}" does not exists`;
        }
    }
    res.send(message);
});

/* --Listen-- */
app.listen(port, () => {
    console.log(`Server running at port ${port}`);
});
